<?php

namespace Source\Models;

use Source\Core\Model;

/**
 * Class Gallery
 * @package Source\Models
 */
class Gallery extends Model
{
    /**
     * Gallery constructor.
     */
    public function __construct()
    {
        parent::__construct("products_gallery", ["id"], ["title"]);
    }
}

