<?php

namespace Source\Models;

use Source\Core\Model;

class Product extends Model
{

    public function __construct()
    {
        parent::__construct("products", ["id"], ["title", "uri", "content", "price"]);
    }

    /**
     * @param null|string $terms
     * @param null|string $params
     * @param string $columns
     * @return mixed|Model
     */
    public function findProduct(?string $terms = null, ?string $params = null, string $columns = "*")
    {
        $terms = "status = :status " . ($terms ? " AND {$terms}" : "");
        $params = "status=post" . ($params ? "&{$params}" : "");
        return parent::find($terms, $params, $columns);
    }

    /**
     * @param string $uri
     * @param string $columns
     * @return null|Product
     */
    public function findByUri(string $uri, string $columns = "*"): ?Product
    {
        $find = $this->find("uri = :uri", "uri={$uri}", $columns);
        return $find->fetch();
    }

    /**
     * @return null|Category
     */
    public function category(): ?Category
    {
        if ($this->category) {
            return (new Category())->findById($this->category);
        }
        return null;
    }

    public function gallery(): ?Gallery
    {
        if ($this->id) {
            return (new Gallery())->find("product_id = :id", "id={$this->id}");
        }
        return null;
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        $checkUri = (new Product())->find("uri = :uri AND id != :id", "uri={$this->uri}&id={$this->id}");

        if ($checkUri->count()) {
            $this->uri = "{$this->uri}-{$this->lastId()}";
        }

        return parent::save();
    }
}