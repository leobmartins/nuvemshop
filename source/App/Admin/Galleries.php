<?php

namespace Source\App\Admin;

use Source\Models\Gallery;
use Source\Support\Pager;
use Source\Support\Thumb;
use Source\Support\Upload;

/**
 * Class Gallery
 * @package Source\App\Admin
 */
class Galleries extends Admin
{
    /** Gallery constructor. */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array|null $data
     */
    public function gallery(?array $data): void
    {
        //Search product gallery with paging
        $gallery = (new Gallery())->find("product_id = :id", "id={$data["product_id"]}");
        $pager = new Pager(url("/admin/products/galleries/"));
        $pager->pager($gallery->count(), 6, (!empty($data["page"]) ? $data["page"] : 1));

        //Opitimization
        $head = $this->seo->render(
            CONF_SITE_NAME . " | Galeria do Produto",
            CONF_SITE_DESC,
            url("/admin"),
            url("/admin/assets/images/image.jpg"),
            false
        );

        //Render view
        echo $this->view->render("widgets/products/gallery", [
            "app" => "products/home",
            "head" => $head,
            "product_id" => $data["product_id"],
            "galleries" => $gallery->order("created_at")->limit($pager->limit())->offset($pager->offset())->fetch(true),
            "paginator" => $pager->render()
        ]);
    }

    /**
     * @param array|null $data
     * @throws \Exception
     */
    public function newGallery(?array $data): void
    {
        //create gallery
        if (!empty($data["action"]) && $data["action"] == "create") {
            //Validation
            $data = array_map("trim", $data);
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

            $galleryCreate = new Gallery();
            $galleryCreate->title = $data["title"];
            $galleryCreate->product_id = $data["product_id"];

            //upload cover
            if (!empty($_FILES["cover"])) {
                $files = $_FILES["cover"];
                $upload = new Upload();
                $image = $upload->image($files, $galleryCreate->title);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $galleryCreate->cover = $image;
            } else {
                $json["message"] = $this->message->error("Por favor envie uma imagem")->render();
                echo json_encode($json);
                return;
            }

            //Save
            if (!$galleryCreate->save()) {
                $json["message"] = $galleryCreate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Imagem criada com sucesso...")->flash();
            $json["redirect"] = url("/admin/products/new-gallery/{$galleryCreate->product_id}/{$galleryCreate->id}");

            echo json_encode($json);
            return;
        }

        //update gallery
        if (!empty($data["action"]) && $data["action"] == "update") {
            //Validation
            $data = array_map("trim", $data);
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

            //Search gallery
            $galleryEdit = (new Gallery())->findById($data["gallery_id"]);
            if (!$galleryEdit) {
                $this->message->error("Você tentou atualizar uma imagem que não existe ou foi removido")->flash();
                echo json_encode(["redirect" => url("/admin/products/home")]);
                return;
            }

            $galleryEdit->title = $data["title"];

            //upload cover
            if (!empty($_FILES["cover"])) {
                if ($galleryEdit->cover && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$galleryEdit->cover}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$galleryEdit->cover}");
                    (new Thumb())->flush($galleryEdit->cover);
                }

                $files = $_FILES["cover"];
                $upload = new Upload();
                $image = $upload->image($files, $galleryEdit->title);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $galleryEdit->cover = $image;
            }

            //Save
            if (!$galleryEdit->save()) {
                $json["message"] = $galleryEdit->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Imagem atualizada com sucesso...")->flash();
            echo json_encode(["reload" => true]);
            return;
        }

        //delete gallery
        if (!empty($data["action"]) && $data["action"] == "delete") {
            //Validation
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

            //Search gallery
            $galleryDelete = (new Gallery())->findById($data["gallery_id"]);
            if (!$galleryDelete) {
                $this->message->error("Você tentou excluir um produto que não existe ou já foi removido")->flash();
                echo json_encode(["reload" => true]);
                return;
            }

            //Delete cover
            if ($galleryDelete->cover && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$galleryDelete->cover}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$galleryDelete->cover}");
                (new Thumb())->flush($galleryDelete->cover);
            }

            //Delete gallery
            $galleryDelete->destroy();
            $this->message->success("Imagem excluída com sucesso...")->flash();

            echo json_encode(["reload" => true]);
            return;
        }

        $galleryEdit = null;
        if (!empty($data["gallery_id"])) {
            $galleryId = filter_var($data["gallery_id"], FILTER_VALIDATE_INT);
            $galleryEdit = (new Gallery())->findById($galleryId);
        }

        //Opitimization
        $head = $this->seo->render(
            CONF_SITE_NAME . " | " . ($galleryEdit->title ?? "Nova Imagem"),
            CONF_SITE_DESC,
            url("/admin"),
            url("/admin/assets/images/image.jpg"),
            false
        );

        //Render view
        echo $this->view->render("widgets/products/new-gallery", [
            "app" => "products/home",
            "head" => $head,
            "product_id" => $data["product_id"],
            "gallery" => $galleryEdit
        ]);
    }
}