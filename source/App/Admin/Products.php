<?php

namespace Source\App\Admin;

use Source\Models\Category;
use Source\Models\Product;
use Source\Support\Pager;
use Source\Support\Thumb;
use Source\Support\Upload;


/**
 * Class Products
 * @package Source\App\Admin
 */
class Products extends Admin
{
    /** Products constructor. */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array|null $data
     */
    public function home(?array $data): void
    {
        //search redirect
        if (!empty($data["s"])) {
            $s = str_search($data["s"]);
            echo json_encode(["redirect" => url("/admin/products/home/{$s}/1")]);
            return;
        }

        //Search products
        $search = null;
        $products = (new Product())->find();

        if (!empty($data["search"]) && str_search($data["search"]) != "all") {
            $search = str_search($data["search"]);
            $products = (new Product())->find("MATCH(title, subtitle) AGAINST(:s)", "s={$search}");
            if (!$products->count()) {
                $this->message->info("Sua pesquisa não retornou resultados")->flash();
                redirect("/admin/products/home");
            }
        }

        //Paging
        $all = ($search ?? "all");
        $pager = new Pager(url("/admin/products/home/{$all}/"));
        $pager->pager($products->count(), 6, (!empty($data["page"]) ? $data["page"] : 1));

        //Opitimization
        $head = $this->seo->render(
            CONF_SITE_NAME . " | Produtos",
            CONF_SITE_DESC,
            url("/admin"),
            url("/admin/assets/images/image.jpg"),
            false
        );

        //Render view
        echo $this->view->render("widgets/products/home", [
            "app" => "products/home",
            "head" => $head,
            "products" => $products->limit($pager->limit())->offset($pager->offset())->order("created_at DESC")->fetch(true),
            "paginator" => $pager->render(),
            "search" => $search
        ]);
    }

    /**
     * @param array|null $data
     * @throws \Exception
     */
    public function product(?array $data): void
    {
        //MCE Upload
        if (!empty($data["upload"]) && !empty($_FILES["image"])) {
            $files = $_FILES["image"];
            $upload = new Upload();
            $image = $upload->image($files, "produto-" . time());

            if (!$image) {
                $json["message"] = $upload->message()->render();
                echo json_encode($json);
                return;
            }

            $json["mce_image"] = '<img style="width: 100%;" src="' . url("/storage/{$image}") . '" alt="{title}" title="{title}">';
            echo json_encode($json);
            return;
        }

        //create product
        if (!empty($data["action"]) && $data["action"] == "create") {
            //Validation
            $data = array_map("trim", $data);

            $content = $data["content"];
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

            $productCreate = new Product();
            $productCreate->category = $data["category"];
            $productCreate->title = $data["title"];
            $productCreate->price = str_replace([".", ","], ["", "."], $data["price"]);
            $productCreate->uri = str_slug($productCreate->title);
            $productCreate->subtitle = $data["subtitle"];
            $productCreate->content = str_replace(["{title}"], [$productCreate->title], $content);
            $productCreate->status = $data["status"];

            //upload cover
            if (!empty($_FILES["cover"])) {
                $files = $_FILES["cover"];
                $upload = new Upload();
                $image = $upload->image($files, $productCreate->title);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $productCreate->cover = $image;
            } else {
                $json["message"] = $this->message->error("Por favor envie uma imagem principal")->render();
                echo json_encode($json);
                return;
            }

            if (!$productCreate->save()) {
                $json["message"] = $productCreate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Produto publicado com sucesso...")->flash();
            $json["redirect"] = url("/admin/products/product/{$productCreate->id}");

            echo json_encode($json);
            return;
        }

        //update product
        if (!empty($data["action"]) && $data["action"] == "update") {
            //Validation
            $data = array_map("trim", $data);
            $content = $data["content"];

            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $productEdit = (new Product())->findById($data["product_id"]);

            if (!$productEdit) {
                $this->message->error("Você tentou atualizar um produto que não existe ou foi removido")->flash();
                echo json_encode(["redirect" => url("/admin/products/home")]);
                return;
            }

            $productEdit->category = $data["category"];
            $productEdit->title = $data["title"];
            $productEdit->uri = str_slug($productEdit->title);
            $productEdit->price = str_replace([".", ","], ["", "."], $data["price"]);
            $productEdit->subtitle = $data["subtitle"];
            $productEdit->content = str_replace(["{title}"], [$productEdit->title], $content);
            $productEdit->status = $data["status"];
            $productEdit->created_at = date_fmt_back($data["created_at"]);

            //upload cover
            if (!empty($_FILES["cover"])) {
                if ($productEdit->cover && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$productEdit->cover}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$productEdit->cover}");
                    (new Thumb())->flush($productEdit->cover);
                }

                $files = $_FILES["cover"];
                $upload = new Upload();
                $image = $upload->image($files, $productEdit->title);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $productEdit->cover = $image;
            }

            if (!$productEdit->save()) {
                $json["message"] = $productEdit->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Produto atualizado com sucesso...")->flash();
            echo json_encode(["reload" => true]);
            return;
        }

        //delete product
        if (!empty($data["action"]) && $data["action"] == "delete") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $productDelete = (new Product())->findById($data["product_id"]);

            if (!$productDelete) {
                $this->message->error("Você tentou excluir um produto que não existe ou já foi removido")->flash();
                echo json_encode(["reload" => true]);
                return;
            }

            if ($productDelete->cover && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$productDelete->cover}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$productDelete->cover}");
                (new Thumb())->flush($productDelete->cover);
            }

            $productDelete->destroy();
            $this->message->success("O Produto foi excluído com sucesso...")->flash();

            echo json_encode(["reload" => true]);
            return;
        }

        $productEdit = null;
        if (!empty($data["product_id"])) {
            $productId = filter_var($data["product_id"], FILTER_VALIDATE_INT);
            $productEdit = (new Product())->findById($productId);
        }

        //Opitimization
        $head = $this->seo->render(
            CONF_SITE_NAME . " | " . ($productEdit->title ?? "Novo Produto"),
            CONF_SITE_DESC,
            url("/admin"),
            url("/admin/assets/images/image.jpg"),
            false
        );

        //Render view
        echo $this->view->render("widgets/products/product", [
            "app" => "products/product",
            "head" => $head,
            "products" => $productEdit,
            "categories" => (new Category())->find("type = :type", "type=post")->order("title")->fetch(true)
        ]);
    }

    /**
     * @param array|null $data
     */
    public function categories(?array $data): void
    {
        //Search category and paging
        $categories = (new Category())->find();
        $pager = new Pager(url("/admin/products/categories/"));
        $pager->pager($categories->count(), 6, (!empty($data["page"]) ? $data["page"] : 1));

        //Opitimization
        $head = $this->seo->render(
            CONF_SITE_NAME . " | Categorias",
            CONF_SITE_DESC,
            url("/admin"),
            url("/admin/assets/images/image.jpg"),
            false
        );

        //Render view
        echo $this->view->render("widgets/products/categories", [
            "app" => "products/categories",
            "head" => $head,
            "categories" => $categories->order("title")->limit($pager->limit())->offset($pager->offset())->fetch(true),
            "paginator" => $pager->render()
        ]);
    }

    /**
     * @param array|null $data
     * @throws \Exception
     */
    public function category(?array $data): void
    {
        //create category
        if (!empty($data["action"]) && $data["action"] == "create") {
            //Validation
            $data = array_map("trim", $data);
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

            $categoryCreate = new Category();
            $categoryCreate->title = $data["title"];
            $categoryCreate->uri = str_slug($categoryCreate->title);
            $categoryCreate->description = $data["description"];

            //upload cover
            if (!empty($_FILES["cover"])) {
                $files = $_FILES["cover"];
                $upload = new Upload();
                $image = $upload->image($files, $categoryCreate->title);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $categoryCreate->cover = $image;
            }

            if (!$categoryCreate->save()) {
                $json["message"] = $categoryCreate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Categoria criada com sucesso...")->flash();
            $json["redirect"] = url("/admin/products/category/{$categoryCreate->id}");

            echo json_encode($json);
            return;
        }

        //update category
        if (!empty($data["action"]) && $data["action"] == "update") {
            //Validation
            $data = array_map("trim", $data);
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

            //Search category
            $categoryEdit = (new Category())->findById($data["category_id"]);
            if (!$categoryEdit) {
                $this->message->error("Você tentou editar uma categoria que não existe ou foi removida")->flash();
                echo json_encode(["redirect" => url("/admin/products/categories")]);
                return;
            }

            $categoryEdit->title = $data["title"];
            $categoryEdit->uri = str_slug($categoryEdit->title);
            $categoryEdit->description = $data["description"];

            //upload cover
            if (!empty($_FILES["cover"])) {
                if ($categoryEdit->cover && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$categoryEdit->cover}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$categoryEdit->cover}");
                    (new Thumb())->flush($categoryEdit->cover);
                }

                $files = $_FILES["cover"];
                $upload = new Upload();
                $image = $upload->image($files, $categoryEdit->title);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $categoryEdit->cover = $image;
            }

            if (!$categoryEdit->save()) {
                $json["message"] = $categoryEdit->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Categoria atualizada com sucesso...")->flash();
            echo json_encode(["reload" => true]);
            return;
        }

        //delete product
        if (!empty($data["action"]) && $data["action"] == "delete") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $categoryDelete = (new Category())->findById($data["category_id"]);

            if (!$categoryDelete) {
                $json["message"] = $this->message->error("A categoria não existe ou já foi excluída antes")->render();
                echo json_encode($json);
                return;
            }

            if ($categoryDelete->products()->count()) {
                $json["message"] = $this->message->warning("Não é possível remover pois existem Produtos cadastrados")->render();
                echo json_encode($json);
                return;
            }

            if ($categoryDelete->cover && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$categoryDelete->cover}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$categoryDelete->cover}");
                (new Thumb())->flush($categoryDelete->cover);
            }

            $categoryDelete->destroy();

            $this->message->success("A categoria foi excluída com sucesso...")->flash();
            echo json_encode(["reload" => true]);

            return;
        }

        $categoryEdit = null;
        if (!empty($data["category_id"])) {
            $categoryId = filter_var($data["category_id"], FILTER_VALIDATE_INT);
            $categoryEdit = (new Category())->findById($categoryId);
        }

        //Opitimization
        $head = $this->seo->render(
            CONF_SITE_NAME . " | Categoria",
            CONF_SITE_DESC,
            url("/admin"),
            url("/admin/assets/images/image.jpg"),
            false
        );

        //Render view
        echo $this->view->render("widgets/products/category", [
            "app" => "products/categories",
            "head" => $head,
            "category" => $categoryEdit
        ]);
    }
}