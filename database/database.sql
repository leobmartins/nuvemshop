-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.1.38-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para nuvemshop
DROP DATABASE IF EXISTS `nuvemshop`;
CREATE DATABASE IF NOT EXISTS `nuvemshop` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `nuvemshop`;

-- Copiando estrutura para tabela nuvemshop.categories
DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `uri` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `cover` varchar(255) DEFAULT NULL,
  `type` varchar(50) NOT NULL DEFAULT 'post' COMMENT 'post, page, etc',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela nuvemshop.categories: ~4 rows (aproximadamente)
DELETE FROM `categories`;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `title`, `uri`, `description`, `cover`, `type`, `created_at`, `updated_at`) VALUES
	(1, 'Camisetas', 'camisetas', 'Encontre as melhores camisetas do mercado com o melhor preço.', 'images/2019/06/camisetas.jpg', 'post', '2018-10-22 15:24:12', '2019-06-09 22:00:29'),
	(2, 'Bermudas', 'bermudas', 'Encontre as melhores bermudas mercado com o melhor preço.', 'images/2019/06/bermudas.jpg', 'post', '2018-11-01 16:32:57', '2019-06-09 22:00:16'),
	(3, 'Tênis', 'tenis', 'Encontre os melhores tênis do mercado com o melhor preço.', 'images/2019/06/tenis.jpg', 'post', '2018-11-01 16:33:05', '2019-06-09 22:00:37');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Copiando estrutura para tabela nuvemshop.mail_queue
DROP TABLE IF EXISTS `mail_queue`;
CREATE TABLE IF NOT EXISTS `mail_queue` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL DEFAULT '',
  `body` text NOT NULL,
  `from_email` varchar(255) NOT NULL DEFAULT '',
  `from_name` varchar(255) NOT NULL DEFAULT '',
  `recipient_email` varchar(255) NOT NULL DEFAULT '',
  `recipient_name` varchar(255) NOT NULL DEFAULT '',
  `sent_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela nuvemshop.mail_queue: ~3 rows (aproximadamente)
DELETE FROM `mail_queue`;
/*!40000 ALTER TABLE `mail_queue` DISABLE KEYS */;
INSERT INTO `mail_queue` (`id`, `subject`, `body`, `from_email`, `from_name`, `recipient_email`, `recipient_name`, `sent_at`, `created_at`, `updated_at`) VALUES
	(1, '[PAGAMENTO CONFIRMADO] Obrigado por assinar o CaféApp', '<!doctype html>\n<html>\n<head>\n    <meta name="viewport" content="width=device-width"/>\n    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>\n    <title>[PAGAMENTO CONFIRMADO] Obrigado por assinar o CaféApp</title>\n    <style>\n        body {\n            -webkit-box-sizing: border-box;\n            -moz-box-sizing: border-box;\n            box-sizing: border-box;\n            font-family: Helvetica, sans-serif;\n        }\n\n        table {\n            max-width: 500px;\n            padding: 0 10px;\n            background: #ffffff;\n        }\n\n        .content {\n            font-size: 16px;\n            margin-bottom: 25px;\n            padding-bottom: 5px;\n            border-bottom: 1px solid #EEEEEE;\n        }\n\n        .content p {\n            margin: 25px 0;\n        }\n\n        .footer {\n            font-size: 14px;\n            color: #888888;\n            font-style: italic;\n        }\n\n        .footer p {\n            margin: 0 0 2px 0;\n        }\n    </style>\n</head>\n<body>\n<table role="presentation" border="0" cellpadding="0" cellspacing="0">\n    <tr>\n        <td>\n            <div class="content">\n                \n<h3>Obrigado Robson!</h3><p>Estamos passando apenas para agradecer por você ser um assinante CaféApp PRO.</p><p>Sua fatura deste mês venceu hoje e já está paga de acordo com seu plano. Qualquer dúvida estamos a disposição.</p>                <p>Atenciosamente, equipe CaféControl.</p>\n            </div>\n            <div class="footer">\n                <p>CaféControl - Gerencie suas contas com o melhor café</p>\n                <p>SC 406 - Rod. Drº Antônio Luiz Moura Gonzaga                    , 3339, Bloco A, Sala 208</p>\n                <p>Florianópolis/SC - 88048-301</p>\n            </div>\n        </td>\n    </tr>\n</table>\n</body>\n</html>\n', 'cursos@upinside.com.br', 'Robson V. Leite', 'robsonvleite@gmail.com', 'Robson Leite', '2019-01-31 14:23:54', '2019-01-04 11:13:11', '2019-02-07 11:57:26'),
	(2, '[PAGAMENTO RECUSADO] Sua conta CaféApp precisa de atenção', '<!doctype html>\n<html>\n<head>\n    <meta name="viewport" content="width=device-width"/>\n    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>\n    <title>[PAGAMENTO RECUSADO] Sua conta CaféApp precisa de atenção</title>\n    <style>\n        body {\n            -webkit-box-sizing: border-box;\n            -moz-box-sizing: border-box;\n            box-sizing: border-box;\n            font-family: Helvetica, sans-serif;\n        }\n\n        table {\n            max-width: 500px;\n            padding: 0 10px;\n            background: #ffffff;\n        }\n\n        .content {\n            font-size: 16px;\n            margin-bottom: 25px;\n            padding-bottom: 5px;\n            border-bottom: 1px solid #EEEEEE;\n        }\n\n        .content p {\n            margin: 25px 0;\n        }\n\n        .footer {\n            font-size: 14px;\n            color: #888888;\n            font-style: italic;\n        }\n\n        .footer p {\n            margin: 0 0 2px 0;\n        }\n    </style>\n</head>\n<body>\n<table role="presentation" border="0" cellpadding="0" cellspacing="0">\n    <tr>\n        <td>\n            <div class="content">\n                \n<h3>Presado Robson!</h3><p>Não conseguimos cobrar seu cartão referênte a fatura deste mês para sua assinatura CaféApp. Precisamos que você veja isso.</p><p>Acesse sua conta para atualizar seus dados de pagamento, você pode cadastrar outro cartão.</p><p>Se não fizer nada agora uma nova tentativa de cobrança será feita em 3 dias. Se não der certo, sua assinatura será cancelada :/</p><p>Qualquer dúvida estamos a disposição.</p>                <p>Atenciosamente, equipe CaféControl.</p>\n            </div>\n            <div class="footer">\n                <p>CaféControl - Gerencie suas contas com o melhor café</p>\n                <p>SC 406 - Rod. Drº Antônio Luiz Moura Gonzaga                    , 3339, Bloco A, Sala 208</p>\n                <p>Florianópolis/SC - 88048-301</p>\n            </div>\n        </td>\n    </tr>\n</table>\n</body>\n</html>\n', 'cursos@upinside.com.br', 'Robson V. Leite', 'robsonvleite@gmail.com', 'Robson Leite', '2019-01-11 12:43:45', '2019-01-04 11:19:54', '2019-02-07 11:57:27'),
	(3, '[ASSINATURA CANCELADA] Sua conta CaféApp agora é FREE', '<!doctype html>\n<html>\n<head>\n    <meta name="viewport" content="width=device-width"/>\n    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>\n    <title>[ASSINATURA CANCELADA] Sua conta CaféApp agora é FREE</title>\n    <style>\n        body {\n            -webkit-box-sizing: border-box;\n            -moz-box-sizing: border-box;\n            box-sizing: border-box;\n            font-family: Helvetica, sans-serif;\n        }\n\n        table {\n            max-width: 500px;\n            padding: 0 10px;\n            background: #ffffff;\n        }\n\n        .content {\n            font-size: 16px;\n            margin-bottom: 25px;\n            padding-bottom: 5px;\n            border-bottom: 1px solid #EEEEEE;\n        }\n\n        .content p {\n            margin: 25px 0;\n        }\n\n        .footer {\n            font-size: 14px;\n            color: #888888;\n            font-style: italic;\n        }\n\n        .footer p {\n            margin: 0 0 2px 0;\n        }\n    </style>\n</head>\n<body>\n<table role="presentation" border="0" cellpadding="0" cellspacing="0">\n    <tr>\n        <td>\n            <div class="content">\n                \n<h3>Que pena Robson :/</h3><p>Tentamos efetuar mais uma cobrança para sua assinatura PRO hoje, mas sem sucesso.</p><p>Como essa já é uma segunda tentativa, infelismente sua assinatura foi cancelada. Mas calma, você pode assinar novamente a qualquer momento.</p><p>Continue controlando com os recursos FREE, e assim que quiser basta assinar novamente e voltar de onde parou :)</p>                <p>Atenciosamente, equipe CaféControl.</p>\n            </div>\n            <div class="footer">\n                <p>CaféControl - Gerencie suas contas com o melhor café</p>\n                <p>SC 406 - Rod. Drº Antônio Luiz Moura Gonzaga                    , 3339, Bloco A, Sala 208</p>\n                <p>Florianópolis/SC - 88048-301</p>\n            </div>\n        </td>\n    </tr>\n</table>\n</body>\n</html>\n', 'cursos@upinside.com.br', 'Robson V. Leite', 'robsonvleite@gmail.com', 'Robson Leite', '2019-01-11 12:43:49', '2019-01-04 11:34:01', '2019-02-07 11:57:28');
/*!40000 ALTER TABLE `mail_queue` ENABLE KEYS */;

-- Copiando estrutura para tabela nuvemshop.products
DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category` int(11) unsigned DEFAULT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `uri` varchar(255) NOT NULL,
  `subtitle` text NOT NULL,
  `content` text NOT NULL,
  `cover` varchar(255) DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `status` varchar(20) NOT NULL DEFAULT 'draft' COMMENT 'post, draft, trash ',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category`),
  FULLTEXT KEY `full_text` (`title`,`subtitle`),
  CONSTRAINT `category_id` FOREIGN KEY (`category`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela nuvemshop.products: ~10 rows (aproximadamente)
DELETE FROM `products`;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `category`, `title`, `uri`, `subtitle`, `content`, `cover`, `views`, `price`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(2, 1, 'Camiseta Compressão Nike Pro Masculina - Preto e Branco', 'camiseta-compressao-nike-pro-masculina-preto-e-branco', 'Conte com a Camiseta Compressão Nike Pro Masculina para treinar na academia e superar seus desafios. Traz tecido respirável e tecnologia Nike Dri-Fit, que afasta o suor da pele para maior conforto.', '<p>Nome: Camiseta Compressão Nike Pro Masculina<br />Gênero: Masculino<br />Indicado para: Dia a Dia<br />Gola: Gola Careca<br />Categoria: Musculação<br />Manga: Manga Curta<br />Material: Poliéster<br />Composição: 92% Poliéster e 8% Elastano<br />Tecnologia: Dri-Fit<br />Definição da Tecnologia: Tecido de desempenho que afasta o suor da pele para ajudá-lo a se manter seco e confortável.<br />Estilo da Peça: Com logo<br />Garantia do Fabricante: Contra Defeito de Fabricação<br />Origem: Estrangeira - Adquirida no mercado interno</p>', 'images/2019/06/crie-um-instalador-de-estrutura-do-banco-de-dados-para-o-seu-cms-utilizando-php-e-ajax.jpg', 16, 42.30, 'post', '2018-10-23 10:41:58', '2019-06-09 22:29:57', NULL),
	(3, 1, 'Camiseta Compressão Nike Pro Manga Longa Masculina - Cinza e Preto', 'camiseta-compressao-nike-pro-manga-longa-masculina-cinza-e-preto', 'A Camiseta Compressão Nike Pro Manga Longa foi feita para potencializar seu desempenho. A peça possui tecido estruturado de poliéster e elastano, que se ajusta ao seu corpo e te mantém fresco.', '<p>Nome: Camiseta Compressão Nike Pro Manga Longa Masculina<br />Gênero: Masculino<br />Indicado para: Dia a Dia<br />Gola: Gola Careca<br />Manga: Manga Longa<br />Material: Poliéster<br />Composição: 92% Poliéster, 8% Elastano<br />Tecnologia: Dri-Fit<br />Definição da Tecnologia: Projetado para melhorar o desempenho atlético em uma variedade de condições e atividades, o Dri-FIT (Tecnologia Inovadora Funcional) é um tecido de desempenho que afasta o suor da pele para ajudá-lo a se manter seco e confortável. Principais Benefícios: A transpiração evapora da superfície do tecido, As propriedades de absorção do suor são permanentes pelo tempo que a roupa durar.<br />Estilo da Peça: Lisa<br />Garantia do Fabricante: Contra Defeito de Fabricação<br />Origem: Estrangeira - Adquirida no mercado interno</p>', 'images/2019/06/camiseta-compressao-nike-pro-manga-longa-masculina-cinza-e-preto.jpg', 8, 42.30, 'post', '2018-10-23 10:41:58', '2019-06-09 21:34:30', NULL),
	(4, 1, 'Camisa De Compressão Térmica Marra 10 Pro Proteção Solar Masculina - Preto', 'camisa-de-compressao-termica-marra-10-pro-protecao-solar-masculina-preto', 'AGORA COM COSTURAS SUPER REFORÇADAS!!! MUITO LEVE Projetada para atender os as especificações profissionais, podem ser usadas sozinhas ou por baixo do uniforme e de outras roupas, proporcionando mais liberdade de movimentos e conforto.', '<p>Fabricada com Lycra® original que acompanha os movimentos do seu corpo, deixando você livre para aproveitar a vida ao máximo! Além disso sua estrutura bioelástica acelera a secagem do suor devido ao fio exclusivo, mantendo a temperatura corporal. Mas não é só isso, feita com Proteção Solar SUN PRO FPU 50+ , atributo proveniente de uma combinação entre estrutura do tecido e acabamentos químicos de última geração, aplicada durante a fiação da malha, persistindo por toda sua vida útil, não saindo com as lavagens e protegendo tanto de UVA como UVB. Definindo essa característica permanentemente, ou seja, não tem risco de sair do produto com o tempo de uso! Com 8% Lycra® em sua composição, oferece maior compressão, conforto e performance. Tem excelente caimento justo ao corpo, tipo segunda pele, com recorte diferenciado com manga raglã. Composição : 92% poliester e 8% Lycra® (elastano). Tipo: Manga longa Gola: Careca (redonda) Peso Aproximado: 90 g (varia de acordo com o tamanho) Origem: Nacional Garantia: Contra defeitos de fabricação Tabela de Dimensões de Tamanhos Aproximados (Fornecido pelo Fabricante) P = 36 cm de largura x 58 cm de altura M = 38 cm de largura x 60 cm de altura G = 40 cm de largura x 62 cm de altura GG = 42 cm de largura x 64 cm de altura 2GG (XG) = 44 cm. de largura x 66 cm de altura</p>\n<p>Nome: Camisa De Compressão Térmica Marra 10 Pro Proteção Solar Masculina<br />Gênero: Masculino<br />Indicado para: Dia a Dia<br />Origem: Nacional</p>', 'images/2019/06/camisa-de-compressao-termica-marra-10-pro-protecao-solar-masculina-preto.jpg', 15, 54.44, 'post', '2018-10-23 10:41:58', '2019-06-09 21:26:44', NULL),
	(5, 1, 'Camisa Térmica Kappa Shadow Masculina - Branco', 'camisa-termica-kappa-shadow-masculina-branco', 'A camisa térmica da linha Shadow apresenta tecido com modelagem mais ajustada', '<p>Fabricada pela Kappa; a camisa térmica da linha Shadow apresenta tecido com modelagem mais ajustada; garantindo máximo conforto e excelente desempenho na prática esportiva. A peça é confeccionada em poliéster e elastano; trazendo design predominantement</p>\n<p>Nome: Camisa Térmica Kappa Shadow Masculina<br />Gênero: Masculino<br />Indicado para: Dia a Dia<br />Origem: Nacional</p>', 'images/2019/06/camisa-termica-kappa-shadow-masculina-branco.jpg', 7, 39.90, 'post', '2018-10-23 10:41:58', '2019-06-09 21:37:04', NULL),
	(6, 3, 'Tênis Kappa Impact Masculino - Preto e Grafite', 'tenis-kappa-impact-masculino-preto-e-grafite', 'Ideal para corredores iniciantes, o Tênis Masculino Kappa Impact possui cabedal com tecido em tramas abertas para circulação de ar durante a corrida, e solado com tecnologia Fluid Gel que absorve os impactos protegendo o ciclo da pisada a cada treino.', '<p>Nome: Tênis Kappa Impact Masculino<br />Gênero: Masculino<br />Indicado para: Dia a Dia<br />Material: Sintético<br />Categoria: Caminhada<br />Composição: Cabedal: Material Sintético. Entressola: EVA. Solado: Borracha com a tecnologia Fluid Gel que absorve os impactos proporcionando maior segurança para sua atividade.<br />Peso do Produto: 320g (o peso do calçado varia de acordo com a numeração)<br />Pisada: Neutra<br />Garantia do Fabricante: Contra Defeito de Fabricação<br />Origem: Nacional</p>', 'images/2019/06/tenis-kappa-impact-masculino-preto-e-grafite.jpg', 2, 59.99, 'post', '2018-10-23 10:41:58', '2019-06-09 21:40:59', NULL),
	(7, 3, 'Tênis Nike Shox Nz Eu Masculino - Preto', 'tenis-nike-shox-nz-eu-masculino-preto', 'Referência no mercado esportivo e casual, a Nike é sinônimo de conforto, estilo e praticidade. Ao longo da história, a marca desenvolveu modelos que se tornaram verdadeiros ícones; um deles é o clássico e atemporal Nike Shox.', '<p>Com um sistema único de amortecimento, composto por quatro colunas adaptáveis com placas moderadoras, o tênis proporciona uma pisada flexível e anatômica. O resultado é um sneaker versártil que acompanha caminhadas, corridas ou simplesmente o lifestyle de quem aposta no visual.</p>\n<p>Nome: Tênis Nike Shox Nz Eu Masculino<br />Gênero: Masculino<br />Indicado para: Dia a Dia<br />Modelo: Nike Shox<br />Estilo da Peça: Com logo<br />Material: Sintético<br />Material Interno: Têxtil<br />Fechamento: Cadarço<br />Linha: Shox<br />Solado: Borracha<br />Definição da Tecnologia: Nike Shox: Colunas que auxiliam na absorção de impactos<br />Peso do Produto: 364g (o peso do produto varia de acordo com a numeraç]ao)<br />Garantia do Fabricante: Contra defeito de fabricação<br />Origem: Estrangeira - Adquirida no mercado interno</p>', 'images/2019/06/tenis-nike-shox-nz-eu-masculino-preto.jpg', 12, 534.99, 'post', '2018-10-23 10:41:58', '2019-06-09 21:43:35', NULL),
	(8, 3, 'Tênis Nike Nightgazer Masculino - Preto e Branco', 'tenis-nike-nightgazer-masculino-preto-e-branco', 'Inspirado nos anos 90, o Tênis Nike Nightgazer Masculino é um sneaker retrô que traz atitude para um look urbano e despojado. Bicolor, seu design revela o estilo de quem está sempre pronto para aproveitar a vida.', '<p>O cabedal confortável traz um misto de couro camurça com mesh para a ventilação dos pés, enquanto a entressola oferece amortecimento durável em poliuretano (PU). Já o solado, recebe uma camada de borracha grossa e resistente com sulcos que facilitam o movimento dos pés. Aposte no clássico Nike Nightgazer e fique pronto para os melhores momentos!</p>\n<p>Nome: Tênis Nike Nightgazer Masculino<br />Gênero: Masculino<br />Indicado para: Dia a Dia<br />Estilo da Peça: Com logo<br />Material: Camurça<br />Altura do Cano: Cano Baixo<br />Fechamento: Cadarço<br />Solado: Borracha<br />Composição: Cabedal: Camurça e Mesh. Solado: Borracha.<br />Garantia do Fabricante: Contra Defeito de Fabricação<br />Origem: Nacional</p>', 'images/2019/06/tenis-nike-nightgazer-masculino-preto-e-branco.jpg', 10, 234.99, 'post', '2018-10-23 10:41:58', '2019-06-09 21:46:15', NULL),
	(9, 3, 'Tênis Adidas Pace Vs Masculino - Branco e Preto', 'tenis-adidas-pace-vs-masculino-branco-e-preto', 'O Tênis Adidas VS Pace Masculino traz no design a atitude urbana e irreverente que vive no DNA da marca. O solado de borracha aderente apresenta sulcos que trabalham os movimentos, enquanto uma palmilha estratégica cuida do conforto interno.', '<p>A lingueta com mesh proporciona ventilação aos pés e exibe logo Adidas no detalhe. Minimalista, o sneaker se destaca pelas 3 listras que estilizam as laterais e representam a assinatura Adidas há gerações. Versátil, combina com todas as ocasiões, sem medo de arriscar.</p>\n<p>Nome: Tênis Adidas Pace Vs Masculino<br />Gênero: Masculino<br />Indicado para: Dia a Dia<br />Estilo da Peça: Com logo<br />Material: Sintético<br />Material Interno: Têxtil<br />Altura do Cano: Cano Baixo<br />Fechamento: Cadarço<br />Solado: Borracha<br />Composição: Cabedal: Sintético / Solado: Borracha.<br />Garantia do Fabricante: Contra Defeito de Fabricação<br />Origem: Estrangeira - Adquirida no mercado interno</p>', 'images/2019/06/tenis-adidas-pace-vs-masculino-branco-e-preto.jpg', 0, 229.90, 'post', '2018-10-23 10:41:58', '2019-06-09 21:48:39', NULL),
	(12, 2, 'Bermuda Nike DRI-FIT Run 7&#34; Masculina - Preto', 'bermuda-nike-dri-fit-run-7-34-masculina-preto', 'Leve, a Bermuda Nike Run Masculina é a companhia perfeita para você pegar pesado na academia. Com ótimo custo-benefício, essa bermuda de treino masculina da Nike possui tecnologia no tecido que afasta o suor da pele para ajudá-lo a se manter seco e confortável, além de bermuda interna e cós com elástico e cordão, proporcionando melhor ajuste ao seu corpo.', '<p>Nome: Bermuda Nike DRI-FIT Run 7" Masculina<br />Gênero: Masculino<br />Importante: Logo refletivo<br />Indicado para: Corrida<br />Estilo da Peça: Lisa<br />Bolso: Externo<br />Tecnologia: Dri-Fit<br />Definição da Tecnologia: Tecido de desempenho que afasta o suor da pele para ajudá-lo a se manter seco e confortável.<br />Bermuda Interna: Com Bermuda Interna<br />Material: Poliéster<br />Composição: Poliéster<br />Cós: Com elástico e cordão<br />Garantia do Fabricante: Contra Defeito de Fabricação<br />Origem: Estrangeira - Adquirida no mercado interno</p>', 'images/2019/06/bermuda-nike-dri-fit-run-7-34-masculina-preto.jpg', 3, 69.99, 'post', '2018-10-23 10:41:58', '2019-06-09 21:53:37', NULL),
	(13, 2, 'Bermuda Nike DRI-FIT Run 7&#34; Masculina - Cinza', 'bermuda-nike-dri-fit-run-7-34-masculina-cinza', 'Leve, a Bermuda Nike Run Masculina é a companhia perfeita para você pegar pesado na academia. Com ótimo custo-benefício, essa bermuda de treino masculina da Nike possui tecnologia no tecido que afasta o suor da pele para ajudá-lo a se manter seco e confortável, além de bermuda interna e cós com elástico e cordão, proporcionando melhor ajuste ao seu corpo.', '<p>Nome: Bermuda Nike DRI-FIT Run 7" Masculina<br />Gênero: Masculino<br />Importante: Logo refletivo<br />Indicado para: Corrida<br />Estilo da Peça: Lisa<br />Bolso: Externo<br />Tecnologia: Dri-Fit<br />Definição da Tecnologia: Tecido de desempenho que afasta o suor da pele para ajudá-lo a se manter seco e confortável.<br />Bermuda Interna: Com Bermuda Interna<br />Material: Poliéster<br />Composição: Poliéster<br />Cós: Com elástico e cordão<br />Garantia do Fabricante: Contra Defeito de Fabricação<br />Origem: Estrangeira - Adquirida no mercado interno</p>', 'images/2019/06/bermuda-nike-dri-fit-run-7-34-masculina-cinza.jpg', 2, 89.99, 'post', '2018-10-23 10:41:58', '2019-06-09 21:59:25', NULL);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Copiando estrutura para tabela nuvemshop.products_gallery
DROP TABLE IF EXISTS `products_gallery`;
CREATE TABLE IF NOT EXISTS `products_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `cover` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `FK_products_gallery_products` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela nuvemshop.products_gallery: ~15 rows (aproximadamente)
DELETE FROM `products_gallery`;
/*!40000 ALTER TABLE `products_gallery` DISABLE KEYS */;
INSERT INTO `products_gallery` (`id`, `product_id`, `title`, `cover`, `created_at`, `updated_at`) VALUES
	(1, 2, 'Costas', 'images/2019/06/teste.jpg', '2019-06-09 18:54:09', '2019-06-09 19:37:12'),
	(2, 2, 'Body', 'images/2019/06/body.jpg', '2019-06-09 19:36:11', '2019-06-09 19:37:01'),
	(3, 2, 'Zoom', 'images/2019/06/zoom.jpg', '2019-06-09 19:39:41', '2019-06-09 19:39:41'),
	(6, 3, 'Costas', 'images/2019/06/costas.jpg', '2019-06-09 20:48:30', '2019-06-09 20:48:30'),
	(7, 3, 'Corpo todo', 'images/2019/06/corpo-todo.jpg', '2019-06-09 20:48:44', '2019-06-09 20:48:44'),
	(8, 3, 'Zoom', 'images/2019/06/zoom-1560124132.jpg', '2019-06-09 20:48:52', '2019-06-09 20:48:52'),
	(9, 4, 'Costas', 'images/2019/06/costas-1560124277.jpg', '2019-06-09 20:51:17', '2019-06-09 20:51:17'),
	(10, 5, 'Lateral', 'images/2019/06/lateral.jpg', '2019-06-09 21:37:49', '2019-06-09 21:37:49'),
	(11, 5, 'Costas', 'images/2019/06/costas-1560127078.jpg', '2019-06-09 21:37:58', '2019-06-09 21:37:58'),
	(12, 5, 'Zoom', 'images/2019/06/zoom-1560127087.jpg', '2019-06-09 21:38:08', '2019-06-09 21:38:08'),
	(13, 6, 'Lateral', 'images/2019/06/lateral-1560127271.jpg', '2019-06-09 21:41:12', '2019-06-09 21:41:12'),
	(14, 6, 'Superior', 'images/2019/06/superior.jpg', '2019-06-09 21:41:22', '2019-06-09 21:41:22'),
	(15, 8, 'Solado', 'images/2019/06/solado.jpg', '2019-06-09 21:46:25', '2019-06-09 21:46:25'),
	(16, 9, 'Costas', 'images/2019/06/costas-1560127728.jpg', '2019-06-09 21:48:48', '2019-06-09 21:48:48'),
	(17, 9, 'Superior', 'images/2019/06/superior-1560127736.jpg', '2019-06-09 21:48:56', '2019-06-09 21:48:56'),
	(18, 9, 'Solado', 'images/2019/06/solado-1560127750.jpg', '2019-06-09 21:49:10', '2019-06-09 21:49:10'),
	(19, 12, 'Corpo', 'images/2019/06/corpo.jpg', '2019-06-09 21:53:49', '2019-06-09 21:53:49');
/*!40000 ALTER TABLE `products_gallery` ENABLE KEYS */;

-- Copiando estrutura para tabela nuvemshop.report_access
DROP TABLE IF EXISTS `report_access`;
CREATE TABLE IF NOT EXISTS `report_access` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `users` int(11) NOT NULL DEFAULT '1',
  `views` int(11) NOT NULL DEFAULT '1',
  `pages` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela nuvemshop.report_access: ~7 rows (aproximadamente)
DELETE FROM `report_access`;
/*!40000 ALTER TABLE `report_access` DISABLE KEYS */;
INSERT INTO `report_access` (`id`, `users`, `views`, `pages`, `created_at`, `updated_at`) VALUES
	(1, 1, 2, 193, '2019-02-11 15:12:15', '2019-02-11 17:57:55'),
	(2, 1, 1, 32, '2019-02-14 13:37:35', '2019-02-14 13:39:23'),
	(3, 1, 3, 130, '2019-03-12 01:18:02', '2019-03-12 12:13:27'),
	(4, 1, 1, 4, '2019-03-15 12:30:52', '2019-03-15 12:33:26'),
	(5, 1, 1, 3, '2019-06-07 21:05:39', '2019-06-07 21:29:09'),
	(6, 1, 1, 148, '2019-06-08 15:55:29', '2019-06-08 20:57:12'),
	(7, 1, 2, 553, '2019-06-09 15:08:46', '2019-06-09 22:44:05'),
	(8, 1, 1, 1, '2019-06-10 23:35:40', '2019-06-10 23:35:40');
/*!40000 ALTER TABLE `report_access` ENABLE KEYS */;

-- Copiando estrutura para tabela nuvemshop.report_online
DROP TABLE IF EXISTS `report_online`;
CREATE TABLE IF NOT EXISTS `report_online` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(11) unsigned DEFAULT NULL,
  `ip` varchar(50) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `agent` varchar(255) NOT NULL DEFAULT '',
  `pages` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela nuvemshop.report_online: ~1 rows (aproximadamente)
DELETE FROM `report_online`;
/*!40000 ALTER TABLE `report_online` DISABLE KEYS */;
/*!40000 ALTER TABLE `report_online` ENABLE KEYS */;

-- Copiando estrutura para tabela nuvemshop.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL DEFAULT '',
  `last_name` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `level` int(11) NOT NULL DEFAULT '1',
  `forget` varchar(255) DEFAULT NULL,
  `genre` varchar(10) DEFAULT NULL,
  `datebirth` date DEFAULT NULL,
  `document` varchar(11) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'registered' COMMENT 'registered, confirmed',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  FULLTEXT KEY `full_text` (`first_name`,`last_name`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela nuvemshop.users: ~1 rows (aproximadamente)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `level`, `forget`, `genre`, `datebirth`, `document`, `photo`, `status`, `created_at`, `updated_at`) VALUES
	(51, 'Developer', 'PHP', 'developer@nuvemshop.com.br', '$2y$10$CL/i2sqA8BValdV1n0goDO71Foso7U4tPdkJcy.FmqEkXCmenQzIa', 5, NULL, 'male', '1992-04-03', '35066825800', 'images/2019/06/developer-php.png', 'confirmed', '2018-12-05 12:42:10', '2019-06-10 23:37:57');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
