<?php $v->layout("_theme"); ?>

    <!-- Products -->
<?php if (empty($products) && !empty($search)): ?>
    <div class="content content">
        <div class="empty_content">
            <h3 class="empty_content_title">Sua pesquisa não retornou resultados :/</h3>
            <p class="empty_content_desc">Você pesquisou por <b><?= $search; ?></b>. Tente outros termos.</p>
            <a class="empty_content_btn gradient gradient-green gradient-hover radius"
               href="<?= url("/produtos"); ?>" title="Produtos">...ou volte aos Produtos</a>
        </div>
    </div>
<?php elseif (empty($products)): ?>
    <div class="content content">
        <div class="empty_content">
            <h3 class="empty_content_title">Ainda estamos trabalhando aqui!</h3>
            <p class="empty_content_desc">Em breve você encontrará os melhores e mais baratos produtos que você já viu.</p>
        </div>
    </div>
<?php else: ?>
    <div class="product_content container content">
        <div class="product_articles">
            <?php foreach ($products as $product): ?>
                <?php $v->insert("product-list", ["product" => $product]); ?>
            <?php endforeach; ?>
        </div>

        <?= $paginator; ?>
    </div>
<?php endif; ?>