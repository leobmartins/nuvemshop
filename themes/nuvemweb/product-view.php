<?php $v->layout("_theme"); ?>
    <article class="product_view">
        <header class="product_view_header">
            <div class="product_view_hero">
                <h1><?= $product->title; ?></h1>
                <div class="product_view_image">
                    <div class="product_view_thumb">
                        <ul class="thumbnails">
                            <li>
                                <a href="<?= image($product->cover, 1280); ?>">
                                    <img src="<?= image($product->cover, 180); ?>" alt="<?= $product->title; ?>"
                                         title="<?= $product->title; ?>">
                                </a>
                            </li>
                            <?php
                            $gallery = $product->gallery()->fetch(true);
                            if($gallery) {
                                foreach ($product->gallery()->fetch(true) as $item) {
                                    echo '<li>
                                          <a href="' . image($item->cover, 1280) . '">
                                             <img src="' . image($item->cover, 180) . '" alt="' . $item->title . '" title="' . $item->title . '">
                                          </a>
                                      </li>';
                                }
                            }
                            ?>
                        </ul>
                    </div>
                    <div class="product_view_cover_large">
                        <div class="main-image">
                            <img class="product_view_cover custom" alt="Placeholder" title="<?= $product->title; ?>"
                                 src="<?= image($product->cover, 1280); ?>"/>
                        </div>
                    </div>
                </div>
                <div class="product_view_meta">
                    <div class="date">&nbsp;</div>
                </div>
            </div>
        </header>
        <div class="product_view_content">
            <div class="htmlchars">
                <div class="product_price">
                    <span>R$ <?= str_price($product->price); ?></span>
                </div>
                <h2><?= $product->subtitle; ?></h2>
                <?= html_entity_decode($product->content); ?>
            </div>

            <aside class="social_share">
                <h3 class="social_share_title icon-heartbeat">Ajude seus amigos a encontrar esta maravilha:</h3>
                <div class="social_share_medias">
                    <!--facebook-->
                    <div class="fb-share-button" data-href="<?= url($product->uri); ?>" data-layout="button_count"
                         data-size="large"
                         data-mobile-iframe="true">
                        <a target="_blank"
                           href="https://www.facebook.com/sharer/sharer.php?u=<?= urlencode(url($product->uri)); ?>"
                           class="fb-xfbml-parse-ignore">Compartilhar</a>
                    </div>

                    <!--twitter-->
                    <a href="https://twitter.com/share?ref_src=site" class="twitter-share-button" data-size="large"
                       data-text="<?= $product->title; ?>" data-url="<?= url($product->uri); ?>"
                       data-via="<?= str_replace("@", "", CONF_SOCIAL_TWITTER_CREATOR); ?>"
                       data-show-count="true">Tweet</a>
                </div>
            </aside>
        </div>
    </article>

<?php $v->start("scripts"); ?>
    <script>
        $(document).ready(function () {
            $('html').on('click', '.thumbnails li', function () {
                var img = $(this).find('a').attr('href');
                $('img.custom').attr('src', img);
                return false;
            });
        });
    </script>
    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v3.1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
<?php $v->end(); ?>