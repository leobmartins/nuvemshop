<article class="product_article">
    <a title="<?= $product->title; ?>" href="<?= url("/produto/{$product->uri}"); ?>">
        <img title="<?= $product->title; ?>" alt="<?= $product->title; ?>"
             src="<?= image($product->cover, 400, 400); ?>"/>
    </a>
    <header>
        <div class="meta">
            <span>Categoria: <?= $product->category()->title; ?></span>
            <span>R$ <?= number_format($product->price, 2, ",", "."); ?></span>
        </div>
        <h2><a title="<?= $product->title; ?>"
               href="<?= url("/produto/{$product->uri}"); ?>"><?= $product->title; ?></a></h2>
        <p><a title="<?= $product->title; ?>" href="<?= url("/produto/{$product->uri}"); ?>">
                <?= str_limit_chars($product->subtitle, 120); ?>
            </a>
        </p>
    </header>
</article>