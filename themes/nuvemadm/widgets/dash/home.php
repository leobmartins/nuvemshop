<?php $v->layout("_admin"); ?>
<?php $v->insert("widgets/dash/sidebar.php"); ?>

<section class="dash_content_app">
    <header class="dash_content_app_header">
        <h2 class="icon-home">Dash</h2>
    </header>

    <div class="dash_content_app_box">
        <section class="app_dash_home_stats">
            <article class="radius">
                <h4><i class="fa fa-cart-arrow-down"></i> Pedidos</h4>
                <p><b><i class="fa fa-check"></i> Aprovados:</b> 0</p>
                <p><b><i class="fa fa-times"></i> Cancelados:</b> 0</p>
                <p><b>R$ Total:</b> R$ 0,00</p>
            </article>

            <article class="radius">
                <h4 class="icon-pencil-square-o">Produtos</h4>
                <p><b>Publicados:</b> <?= $product->products; ?></p>
                <p><b>Rascunhos:</b> <?= $product->drafts; ?></p>
                <p><b>Categorias:</b> <?= $product->categories; ?></p>
            </article>

            <article class="users radius">
                <h4 class="icon-user">Usuários</h4>
                <p><b>Usuários:</b> <?= $users->users; ?></p>
                <p><b>Admins:</b> <?= $users->admins; ?></p>
            </article>
        </section>
    </div>
</section>

<?php $v->start("scripts"); ?>
<?php $v->end(); ?>
