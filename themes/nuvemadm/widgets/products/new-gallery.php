<?php $v->layout("_admin"); ?>
<?php $v->insert("widgets/products/sidebar.php"); ?>

<section class="dash_content_app">
    <?php if (!$gallery): ?>
        <header class="dash_content_app_header">
            <h2 class="icon-plus-circle">Nova Imagem</h2>
            <a class="icon-plus-circle btn btn-green" href="<?= url("/admin/products/galleries/{$product_id}"); ?>">Ver Imagens</a>
        </header>

        <div class="dash_content_app_box">
            <form class="app_form" action="<?= url("/admin/products/new-gallery/{$product_id}/"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="create"/>

                <label class="label">
                    <span class="legend">*Título:</span>
                    <input type="text" name="title" value="" placeholder="O nome da imagem" required/>
                </label>

                <label class="label">
                    <span class="legend">Imagem:</span>
                    <input type="file" name="cover" class="wc_loadimage" placeholder="Uma imagem do produto"/>
                </label>

                <div class="al-right">
                    <button class="btn btn-green icon-check-square-o">Criar Imagem</button>
                </div>
            </form>
        </div>
    <?php else: ?>
        <header class="dash_content_app_header">
            <h2 class="icon-bookmark-o"><?= $gallery->title; ?></h2>
            <a class="icon-camera btn btn-green" href="<?= url("/admin/products/galleries/{$product_id}"); ?>">Ver Imagens</a>
        </header>

        <div class="dash_content_app_box">
            <form class="app_form" action="<?= url("/admin/products/new-gallery/{$product_id}/{$gallery->id}"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="update"/>

                <label class="label">
                    <span class="legend">*Título:</span>
                    <input type="text" name="title" value="<?= $gallery->title; ?>" placeholder="O nome da imagem" required/>
                </label>

                <label class="label">
                    <span class="legend">Imagem:</span>
                    <input type="file" name="cover" class="wc_loadimage" placeholder="Uma imagem do produto"/>
                </label>

                <div class="al-right">
                    <button class="btn btn-blue icon-check-square-o">Atualizar</button>
                </div>
            </form>
        </div>
    <?php endif; ?>
</section>