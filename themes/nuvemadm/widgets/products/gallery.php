<?php $v->layout("_admin"); ?>
<?php $v->insert("widgets/products/sidebar.php"); ?>

<section class="dash_content_app">
    <header class="dash_content_app_header">
        <h2 class="icon-camera">Imagens</h2>
        <a class="icon-pencil-square-o btn btn-blue" href="<?= url("/admin/products/product/{$product_id}"); ?>">Voltar</a>
        <a class="icon-plus-circle btn btn-green" href="<?= url("/admin/products/new-gallery/{$product_id}"); ?>">Nova
            Imagem</a>
    </header>

    <div class="dash_content_app_box">
        <section>
            <div class="app_blog_home">
                <?php
                if (!$galleries): ?>
                    <div class="message info icon-info">Ainda não existem Imagens cadastradas para este produto.</div>
                <?php else: ?>
                    <?php foreach ($galleries as $gallery):
                        $galleryCover = ($gallery->cover ? image($gallery->cover, 300,300) : image("images/no_image.jpg", 300,300));
                        ?>
                        <article>
                            <div style="background-image: url(<?= $galleryCover; ?>);background-position: center;height: 250px;"
                                 class="cover embed radius"></div>
                            <h3 class="tittle">
                                <span class="icon-check"><?= $gallery->title; ?></span>
                            </h3>

                            <div class="actions">
                                <a class="icon-pencil btn btn-blue" title=""
                                   href="<?= url("/admin/products/new-gallery/{$product_id}/{$gallery->id}"); ?>">Editar</a>

                                <a class="icon-trash-o btn btn-red" title="" href="#"
                                   data-post="<?= url("/admin/products/new-gallery/{$product_id}/{$gallery->id}"); ?>"
                                   data-action="delete"
                                   data-confirm="Tem certeza que deseja deletar essa Imagem?"
                                   data-gallery_id="<?= $gallery->id; ?>">Deletar</a>
                            </div>
                        </article>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>

            <?= $paginator; ?>
        </section>
    </div>
</section>