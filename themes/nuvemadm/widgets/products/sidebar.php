<div class="dash_content_sidebar">
    <h3 class="icon-asterisk">Dashboard/Produtos</h3>
    <p class="dash_content_sidebar_desc">Aqui você gerencia todos os produtos do site.</p>

    <nav>
        <?php
        $nav = function ($icon, $href, $title) use ($app) {
            $active = ($app == $href ? "active" : null);
            $url = url("/admin/{$href}");
            return "<a class=\"icon-{$icon} radius {$active}\" href=\"{$url}\">{$title}</a>";
        };

        echo $nav("pencil-square-o", "products/home", "Produtos");
        echo $nav("bookmark", "products/categories", "Categorias");
        echo $nav("plus-circle", "products/product", "Novo Produto");
        ?>

        <?php if (!empty($products->cover)): ?>
            <img class="radius cover" style="width: 100%; margin-top: 30px" src="<?= image($products->cover, 680); ?>"/>
            <a href="<?= url("/admin/products/galleries/{$products->id}"); ?>">
                <span class="btn btn-blue icon-plus-circle">Mais Fotos</span>
            </a>
        <?php endif; ?>

        <?php if (!empty($category->cover)): ?>
            <img class=" radius cover" style="width: 100%; margin-top: 30px"
                 src="<?= image($category->cover, 680); ?>"/>
        <?php endif; ?>

        <?php if (!empty($gallery->cover)): ?>
            <img class=" radius cover" style="width: 100%; margin-top: 30px"
                 src="<?= image($gallery->cover, 680); ?>"/>
        <?php endif; ?>
    </nav>
</div>