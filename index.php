<?php

ob_start();
require __DIR__ . "/vendor/autoload.php";

/** BOOTSTRAP */
use CoffeeCode\Router\Router;
use Source\Core\Session;

$session = new Session();
$route = new Router(url(), ":");

$route->namespace("Source\App");

/** WEB ROUTES */
$route->group(null);
$route->get("/", "Web:home");
$route->get("/pagina/{page}", "Web:home");
$route->get("/produto/{uri}", "Web:product");

/** ADMIN ROUTES */
$route->namespace("Source\App\Admin");
$route->group("/admin");

//login
$route->get("/", "Login:root");
$route->get("/login", "Login:login");
$route->post("/login", "Login:login");

//dash
$route->get("/dash", "Dash:dash");
$route->get("/dash/home", "Dash:home");
$route->post("/dash/home", "Dash:home");
$route->get("/logoff", "Dash:logoff");

//products
$route->get("/products/home", "Products:home");
$route->post("/products/home", "Products:home");
$route->get("/products/home/{search}/{page}", "Products:home");
$route->get("/products/product", "Products:product");
$route->post("/products/product", "Products:product");
$route->get("/products/product/{product_id}", "Products:product");
$route->post("/products/product/{product_id}", "Products:product");

//categories
$route->get("/products/categories", "Products:categories");
$route->get("/products/categories/{page}", "Products:categories");
$route->get("/products/category", "Products:category");
$route->post("/products/category", "Products:category");
$route->get("/products/category/{category_id}", "Products:category");
$route->post("/products/category/{category_id}", "Products:category");

//galleries
$route->get("/products/galleries/{product_id}", "Galleries:gallery");
$route->get("/products/new-gallery/{product_id}", "Galleries:newGallery");
$route->get("/products/new-gallery/{product_id}/{gallery_id}", "Galleries:newGallery");
$route->post("/products/new-gallery/{product_id}/", "Galleries:newGallery");
$route->post("/products/new-gallery/{product_id}/{gallery_id}", "Galleries:newGallery");

//users
$route->get("/users/home", "Users:home");
$route->post("/users/home", "Users:home");
$route->get("/users/home/{search}/{page}", "Users:home");
$route->get("/users/user", "Users:user");
$route->post("/users/user", "Users:user");
$route->get("/users/user/{user_id}", "Users:user");
$route->post("/users/user/{user_id}", "Users:user");

//END ADMIN
$route->namespace("Source\App");

/** ERROR ROUTES */
$route->group("/ops");
$route->get("/{errcode}", "Web:error");

/** ROUTE */
$route->dispatch();

/** ERROR REDIRECT */
if ($route->error()) {
    $route->redirect("/ops/{$route->error()}");
}

ob_end_flush();